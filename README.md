# BMI comparison script

Coding challenge # 1 from The Complete JavaScript Course 2019 on Udemy

Mark and John are trying to compare their BMI, which is calculated using this formula: 
BMI = mass / height^2 = mass / (height * height). (mass in kg and height in meter).

