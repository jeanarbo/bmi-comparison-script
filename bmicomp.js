var markHeight = 1.72; // Height in meters = 5'6in
var markMass = 68.03;  // Weight in kilograms = 160lbs
var johnHeight = 1.90; // Height in meteres = 6'2in
var johnMass = 95.25; // Weight kilograms = 210lbs

var markBmi = markMass / (markHeight * markHeight); // Divides mark's mass by his height times 2 and stores the results in the markBmi variable
var johnBmi = johnMass / (johnHeight * johnHeight); // Divides john's mass by his height times 2 and stores the results in the johnBmi variable

var higherBmi = markBmi >= johnBmi; // Determines if Marks BMI is greater than or equal to John's BMI and stores the results in the higherBmi variable

console.log ("Is Mark's BMI higher than John's?" + ' ' + higherBmi); // logs the string to the console with the higherBmi varibles results